FROM mcr.microsoft.com/dotnet/core/sdk:2.2 AS build-env
WORKDIR /app

COPY ./Transmissions.Api/*.csproj ./Transmissions.Api/
COPY ./Transmissions.Domain/*.csproj ./Transmissions.Domain/

WORKDIR /app/Transmissions.Api

RUN dotnet restore

COPY . /app/
RUN dotnet publish -c Release -o out

FROM mcr.microsoft.com/dotnet/core/aspnet:2.2
WORKDIR /app
COPY --from=build-env /app/Transmissions.Api/out .

ENTRYPOINT ["dotnet", "Transmissions.Api.dll"]